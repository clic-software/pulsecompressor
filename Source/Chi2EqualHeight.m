function chi2 = Chi2EqualHeight(x,pulse,cavity)
% Goal function for finding the pulse separation that results in 
% equal energy between the two pulses

pulse.deltaT = x;
t = pulse.t;
[U, indsTop] = GenerateInputPulse(pulse);
[Pin, Pout] = CavityOutput(U, t, cavity);

E1 = trapz(t(indsTop(2,:)), Pout(indsTop(2,:)));
E2 = trapz(t(indsTop(1,:)), Pout(indsTop(1,:)));

chi2 = abs((E1-E2)/(0.5*(E1+E2)));
end

