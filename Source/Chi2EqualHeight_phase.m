function chi2 = Chi2EqualHeight_phase(x,pulse,cavity,pulseNum)
% Goal function for finding the pulse separation that results in 
% equal energy between the two pulses by means of adjusting the phase 
% in pulse pulseNum

if pulseNum == 1
  pulse.phin = x;
  pulse.Phb = x;
elseif pulseNum == 2
  pulse.phin2 = x;
  pulse.Phb2 = x;
end

t = pulse.t;
[U, indsTop] = GenerateInputPulse(pulse);
[Pin, Pout] = CavityOutput(U, t, cavity);

E1 = trapz(t(indsTop(2,:)), Pout(indsTop(2,:)));
E2 = trapz(t(indsTop(1,:)), Pout(indsTop(1,:)));

chi2 = abs((E1-E2)/(0.5*(E1+E2)));
end

