function [U1, indsTop]  = GenerateInputPulse(pulse)
if ~isfield(pulse,'deltaT')
    %% Generate single pulse:
    t = pulse.t;
    TKp = pulse.TKp;    % klystron pulse length, ns
    tp = pulse.tp;      % pulse length
    phin = pulse.phin;  % start of exponential ramp
    af = pulse.af;      % curvature
    Phb = pulse.Phb;    % end of phase
    
    te1 = 100;
    tst = TKp - tp - te1;
    inds = 1:length(t);
    indsTop = inds(logical((t > tst + te1 + 20).*(t <= TKp)));

    % phase
    f = zeros(size(t));
    logvec1 = logical((t >= tst).*(t < tst + te1));
    logvec2 = logical((t >= tst + te1).*(t <= TKp));
    logvec3 = logical((t >= TKp).*(t <= t(end)));
    f(logvec1) = (t(logvec1) - tst)*phin/te1;
    ttemp = t(logvec2) - tst - te1;
    f(logvec2) = phin + (exp(ttemp*af/ttemp(end)) - 1)*(Phb-phin)/(exp(af)-1);        
    f(logvec3) = Phb*ones(size(f(logvec3)));

else  
    %% Generate double pulse:
    t = pulse.t;
    TKp = pulse.TKp; % klystron pulse length, ns
    tp = pulse.tp; % pulse length
    
    phin = pulse.phin; 
    phin2 = pulse.phin2;
    af = pulse.af;    
    af2 = pulse.af2;    
    Phb = pulse.Phb;
    Phb2 = pulse.Phb2;
    deltaT = pulse.deltaT;
    
    te1 = 100;    
    tst = TKp - tp - te1;
    inds = 1:length(t);
    indsTop = inds(logical((t > tst + te1).*(t <= TKp)));

    tst2 = TKp - tp - 2*te1 - deltaT;
    inds2 = inds(logical((t > tst2 + te1).*(t <= tst2 + te1 + tp)));
    m1 = length(indsTop);
    m2 = length(inds2);
    if m2 > m1
        indsTop = [indsTop; inds2(1:length(indsTop))];
    else
        indsTop = [indsTop(1:length(inds2)); inds2];
    end
    
    % phase
    f = zeros(size(t));
    logvec1 = logical((t >= tst).*(t < tst + te1));
    logvec2 = logical((t >= tst + te1).*(t <= TKp));    
    logvec3 = logical((t >= TKp).*(t <= t(end)));
    f(logvec1) = (t(logvec1) - tst)*phin/te1;
    ttemp = t(logvec2) - tst - te1;
    f(logvec2) = phin + (exp(ttemp*af/ttemp(end)) - 1)*(Phb-phin)/(exp(af)-1);
    ttemp = t(logvec3) - TKp;   
    f(logvec3) = Phb*ones(size(f(logvec3)));
    
    te2 = 100;
    % add pulse before:
    logvec4 = logical((t >= tst2).*(t < tst2 + te1));
    logvec5 = logical((t >= tst2 + te1).*(t <= tst2 + te1 + tp));    
    logvec6 = logical((t > tst2 + te1 + tp).*(t <= tst2 + te1 + te2 + tp));    
    f(logvec4) = (t(logvec4) - tst2)*phin2/te1;
    ttemp = t(logvec5) - tst2 - te1;
    f(logvec5) = phin2 + (exp(ttemp*af2/ttemp(end)) - 1)*(Phb2-phin2)/(exp(af2)-1);
    ttemp = t(logvec6) - tst2 - te1 - tp;
    f(logvec6) = Phb2 - Phb2*ttemp/ttemp(end);   
    
end

te = 50;
% amplitude
u = zeros(size(t));
logvec1u = t <= te; 
logvec2u = logical((t > te).*(t < TKp));
logvec3u = logical((t >= TKp).*(t < TKp + te));
u(logvec1u) = t(logvec1u)/te;
u(logvec2u) = ones(size(t(logvec2u)));
u(logvec3u) = 1 - (t(logvec3u) - TKp)/te;

% if amplitude is modulated
if isfield(pulse,'Amod')
  Amod = pulse.Amod;
  u = u.*Amod;
end

% signal in complex form
U1 = u.*exp(1i*f*pi/180);  

end

