function chi2 = Chi2FlatPulse(x,pulse,cavity)
% Goal function for optimizing the flatness of the compressed pulse
phin = x(1);
af = x(2);
pulse.phin = phin;
pulse.af = af;

[U, indsTop] = GenerateInputPulse(pulse);
[Pin, Pout] = CavityOutput(U, pulse.t, cavity);
inds = indsTop;

if phin < 50 || phin > 170 || af < 1e-3 || af > 5
    chi2 = 1e3;
else
    chi2 = sum((Pout(inds)-mean(Pout(inds))).^2/mean(Pout(inds)));
end        
end