function [ ] = plotSignals_power(t, Pin, Pout, indsTop)
%% Plot only power signals
figure
plot(t, Pin, 'Linewidth', 2, 'Color', 'b');
hold on;
plot(t, Pout, 'Linewidth', 2, 'Color', 'r');
maxY = max(get(gca,'YLim'));
maxX = max(get(gca,'XLim'));
if min(size(indsTop)) == 1
  plot([t(indsTop(1)), t(indsTop(1))], [0, maxY], 'k--');
  plot([t(indsTop(end)), t(indsTop(end))], [0, maxY], 'k--');
  plot([0, maxX], [mean(Pout(indsTop)), mean(Pout(indsTop))], 'k--');
else
  plot([t(indsTop(1,1)), t(indsTop(1,1))], [0, maxY], 'k--');
  plot([t(indsTop(2,1)), t(indsTop(2,1))], [0, maxY], 'k--');
  plot([t(indsTop(1,end)), t(indsTop(1,end))], [0, maxY], 'k--');
  plot([t(indsTop(2,end)), t(indsTop(2,end))], [0, maxY], 'k--');
  plot([0, maxX], [mean(Pout(indsTop(1,:))), mean(Pout(indsTop(1,:)))], 'k--');
  plot([0, maxX], [mean(Pout(indsTop(2,:))), mean(Pout(indsTop(2,:)))], 'k--');
end
hold off;
hx = xlabel('time [ns]'); 
hy = ylabel('Power');
hl = legend('Input', 'Output', 'Location', 'NorthWest');
set([gca,hx,hy,hl], 'Fontsize', 15);  

end
