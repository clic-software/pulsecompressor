function [Pin, Pout] = CavityOutput(U, t, cavity) 
CVc = CavityResponse(cavity);
dT = t(2)-t(1);

%% Fourier of pulse:
omega = (cavity.fh - cavity.Fc)*2*pi;
F = zeros(size(omega));

for k = 1:length(F)
    F(k) = 1/sqrt(2*pi)*sum(U.*exp(-1i*omega(k).*t*1e-9)*dT*1e-9);
end

%% Output signal:
W = zeros(size(t));
for k = 1:length(W)
    W(k) = 1/sqrt(2*pi)*sum(F.*CVc.*exp(1i*omega.*t(k)*1e-9)*(omega(2)-omega(1)));
end

Pin = real(U).^2;
Pout = real(W).^2;

end

