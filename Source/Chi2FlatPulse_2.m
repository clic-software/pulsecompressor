function chi2 = Chi2FlatPulse_2(x,pulse,cavity)
% Goal function for optimizing the flatness of the compressed pulse
phin2 = x(1);
af2 = x(2);
pulse.phin2 = phin2;
pulse.af2 = af2;

[U, indsTop] = GenerateInputPulse(pulse);
[Pin, Pout] = CavityOutput(U, pulse.t, cavity);

if phin2 < 50 || phin2 > 170 || af2 < 1e-3 || af2 > 5
    chi2 = 1e3;
else
    chi2 = sum((Pout(indsTop(2,:))-mean(Pout(indsTop(2,:)))).^2/mean(Pout(indsTop(2,:))));
end        
end