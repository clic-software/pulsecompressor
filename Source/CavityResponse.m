function CavResp = CavityResponse(cavity)
% 
Fc = cavity.Fc; % cavity frequency
Q0 = cavity.Q0;
Beta = cavity.Beta;
fh = cavity.fh;

% cavity amplitude:
AA = 2-2*Beta/(Beta+1);
BB = 1-1i*2*Q0*(fh-Fc)/((Beta+1)*Fc);
CC = 1 + (2*Q0*(fh-Fc)/((Beta+1)*Fc)).^2;
At = 1 - AA*BB./CC;

At1 = 1 - At;
At2 = atan2(imag(At1), real(At1))*2;
CavResp = abs(At).*exp(1i*At2);
end

