function [pulse, cavity] = OptimizeDoublePulse(pulseLength, frequency, klystronPulse, deltaTin, BetaIn)
% This script is originally based I. Syratchev's Mathcad tool
% Written by J. Ogren, 2018

% Optimize the compression for a single pulse. For a given pulse length:
% 1) optimize beta to find the highest peak of the compressed pulse
% 2) find a ramp of the phase to achieve a flat pulse

switch nargin
case 3
%% Define klystron pulse
pulse.tp = pulseLength;           % pulse length compressed pulse, ns
pulse.t = 5*1e-9*(0:2000)*1e9;    % time vector
pulse.TKp = klystronPulse;        % klystron pulse length, ns
pulse.phin = 180;                 % Start of exponential ramp
pulse.af = 1e-5;                  % "Curvature"
pulse.phin2 = 180;                % Start of exponential ramp
pulse.af2 = 1e-5;                 % "Curvature"
pulse.deltaT = 2000;              % separation between the two pulses
pulse.Phb2 = 180;                 % Final phase of former pulse
pulse.Phb =  180;                 % Final phase of latter pulse

[U, indsTop] = GenerateInputPulse(pulse);

%% Define cavity
cavity.Fc = frequency; % cavity frequency
cavity.Q0 = 1.8e5;
cavity.Beta = 7.8;
cavity.fh = cavity.Fc - 50e6 + (0:2000)*0.050e6;

%% Generate output and plot
[Pin, Pout] = CavityOutput(pulse, cavity);

compression_1 = mean(Pout(indsTop(2,:)));
compression_2 = mean(Pout(indsTop(1,:)));

%% Adjust beta to maximize energy in the two pulses
%% Optimize Beta
fprintf('\nOptimizing coupling...\n');
[Beta_opt,fval] = fminbnd(@(x) Chi2Beta(x,pulse,cavity),1,25)

cavity.Beta = Beta_opt;
[Pin, Pout] = CavityOutput(pulse, cavity);

t = pulse.t;
compression_1 = mean(Pout(indsTop(2,:)));
compression_2 = mean(Pout(indsTop(1,:)));

%% Adjust deltaT to make pulses equal:
fprintf('Find pulse separation to make pulses equal...\n');
LB = 100; % 100 ns lower boundary
UB = pulse.TKp - 2*pulse.tp - 100; % upper boundary
[deltaT_opt,fval] = fminbnd(@(x) Chi2EqualHeight(x,pulse,cavity),LB,UB)
pulse.deltaT = deltaT_opt;

[Pin, Pout, U, indsTop] = CavityOutput(pulse, cavity);

compression_1 = mean(Pout(indsTop(2,:)));
compression_2 = mean(Pout(indsTop(1,:)));


%% Flatten the former pulse: 2 variable optimum phase ramp
fprintf('Flattening of the former pulse...\n');
x0 = [130, 2]; % start value
[x_opt,fval] = fminsearch(@(x) Chi2FlatPulse_2(x,pulse,cavity),x0);

pulse.phin2 = x_opt(1);
pulse.af2 = x_opt(2);
[Pin, Pout, U, indsTop] = CavityOutput(pulse, cavity);

compression_1 = mean(Pout(indsTop(2,:)));
compression_2 = mean(Pout(indsTop(1,:)));

%% Flatten the later pulse: 2 variable optimum phase ramp
fprintf('Flattening of the latter pulse...\n');
x0 = [130, 2]; % start value
[x_opt,fval] = fminsearch(@(x) Chi2FlatPulse(x,pulse,cavity),x0);

pulse.phin = x_opt(1);
pulse.af = x_opt(2);
[Pin, Pout, U, indsTop] = CavityOutput(pulse, cavity);

compression_1 = mean(Pout(indsTop(2,:)));
compression_2 = mean(Pout(indsTop(1,:)));

t = pulse.t;
Ein = trapz(t, Pin);
E1 = trapz(t(indsTop(2,:)), Pout(indsTop(2,:)));
E2 = trapz(t(indsTop(1,:)), Pout(indsTop(1,:))); 
Eout = E1 + E2;
Efficiency = Eout/Ein;

%% Adjust deltaT to make pulses equal:
fprintf('Find pulse separation to make flat pulses equal...\n');
LB = 0.8*pulse.deltaT;
UB = 1.2*pulse.deltaT;
[deltaT_opt,fval] = fminbnd(@(x) Chi2EqualHeight(x,pulse,cavity),LB,UB)
pulse.deltaT = deltaT_opt;

[Pin, Pout, U, indsTop] = CavityOutput(pulse, cavity);

compression_1 = mean(Pout(indsTop(2,:)))
compression_2 = mean(Pout(indsTop(1,:)))

%%
case 4
%% Define klystron pulse
pulse.tp = pulseLength;           % pulse length compressed pulse, ns
pulse.t = 5*1e-9*(0:2000)*1e9;    % time vector
pulse.TKp = klystronPulse;        % klystron pulse length, ns
pulse.phin = 180;                 % Start of exponential ramp
pulse.af = 1e-5;                  % "Curvature"
pulse.phin2 = 180;                % Start of exponential ramp
pulse.af2 = 1e-5;                 % "Curvature"
pulse.deltaT = deltaTin;          % separation between the two pulses
pulse.Phb2 = 180;                 % Final phase of former pulse
pulse.Phb =  180;                 % Final phase of latter pulse

[U, indsTop] = GenerateInputPulse(pulse);

%% Define cavity
cavity.Fc = frequency; % cavity frequency
cavity.Q0 = 1.8e5;
cavity.Beta = 8;
cavity.fh = cavity.Fc - 50e6 + (0:2000)*0.050e6;

%% Adjust beta to maximize energy in the two pulses
%% Optimize Beta
fprintf('\nOptimizing coupling...\n');
[Beta_opt,fval] = fminbnd(@(x) Chi2Beta(x,pulse,cavity),1,25)

cavity.Beta = Beta_opt;
[Pin, Pout] = CavityOutput(pulse, cavity);

%% adjust phase ramp
[Pin, Pout, U, indsTop] = CavityOutput(pulse, cavity);

t = pulse.t;
E2 = trapz(t(indsTop(2,:)), Pout(indsTop(2,:)));
E1 = trapz(t(indsTop(1,:)), Pout(indsTop(1,:))); 

LB = 90; % 90 degree lower boundary
UB = 180; % upper boundary
if E2 > E1
  [phase_opt,fval] = fminbnd(@(x) Chi2EqualHeight_phase(x,pulse,cavity,2),LB,UB)
  pulse.phin2 = phase_opt;
  pulse.Phb2 = phase_opt;
else
  [phase_opt,fval] = fminbnd(@(x) Chi2EqualHeight_phase(x,pulse,cavity,1),LB,UB)
  pulse.phin = phase_opt;
  pulse.Phb = phase_opt;
end

%% Flatten the former pulse: 2 variable optimum phase ramp
fprintf('Flattening of the former pulse...\n');
x0 = [130, 2]; % start value
[x_opt,fval] = fminsearch(@(x) Chi2FlatPulse_2(x,pulse,cavity),x0);

pulse.phin2 = x_opt(1);
pulse.af2 = x_opt(2);
[Pin, Pout, U, indsTop] = CavityOutput(pulse, cavity);

compression_1 = mean(Pout(indsTop(2,:)));
compression_2 = mean(Pout(indsTop(1,:)));

%% Flatten the later pulse: 2 variable optimum phase ramp
fprintf('Flattening of the latter pulse...\n');
x0 = [130, 2]; % start value
[x_opt,fval] = fminsearch(@(x) Chi2FlatPulse(x,pulse,cavity),x0);

pulse.phin = x_opt(1);
pulse.af = x_opt(2);
[Pin, Pout, U, indsTop] = CavityOutput(pulse, cavity);

compression_1 = mean(Pout(indsTop(2,:)));
compression_2 = mean(Pout(indsTop(1,:)));

t = pulse.t;
Ein = trapz(t, Pin);
E1 = trapz(t(indsTop(2,:)), Pout(indsTop(2,:)));
E2 = trapz(t(indsTop(1,:)), Pout(indsTop(1,:))); 
Eout = E1 + E2;
Efficiency = Eout/Ein;

compression_1 = mean(Pout(indsTop(2,:)))
compression_2 = mean(Pout(indsTop(1,:)))


%%
case 5
%% Define klystron pulse
pulse.tp = pulseLength;           % pulse length compressed pulse, ns
pulse.t = 5*1e-9*(0:2000)*1e9;    % time vector
pulse.TKp = klystronPulse;        % klystron pulse length, ns
pulse.phin = 180;                 % Start of exponential ramp
pulse.af = 1e-5;                  % "Curvature"
pulse.phin2 = 180;                % Start of exponential ramp
pulse.af2 = 1e-5;                 % "Curvature"
pulse.deltaT = deltaTin;          % separation between the two pulses
pulse.Phb2 = 180;                 % Final phase of former pulse
pulse.Phb =  180;                 % Final phase of latter pulse

[U, indsTop] = GenerateInputPulse(pulse);

%% Define cavity
cavity.Fc = frequency; % cavity frequency
cavity.Q0 = 1.8e5;
cavity.Beta = BetaIn;
cavity.fh = cavity.Fc - 50e6 + (0:2000)*0.050e6;

%% adjust phase ramp
[Pin, Pout, U, indsTop] = CavityOutput(pulse, cavity);

t = pulse.t;
E2 = trapz(t(indsTop(2,:)), Pout(indsTop(2,:)));
E1 = trapz(t(indsTop(1,:)), Pout(indsTop(1,:))); 

LB = 90; % 90 degree lower boundary
UB = 180; % upper boundary
if E2 > E1
  [phase_opt,fval] = fminbnd(@(x) Chi2EqualHeight_phase(x,pulse,cavity,2),LB,UB)
  pulse.phin2 = phase_opt;
  pulse.Phb2 = phase_opt;
else
  [phase_opt,fval] = fminbnd(@(x) Chi2EqualHeight_phase(x,pulse,cavity,1),LB,UB)
  pulse.phin = phase_opt;
  pulse.Phb = phase_opt;
end

%% Flatten the former pulse: 2 variable optimum phase ramp
fprintf('Flattening of the former pulse...\n');
x0 = [130, 2]; % start value
[x_opt,fval] = fminsearch(@(x) Chi2FlatPulse_2(x,pulse,cavity),x0);

pulse.phin2 = x_opt(1);
pulse.af2 = x_opt(2);
[Pin, Pout, U, indsTop] = CavityOutput(pulse, cavity);

compression_1 = mean(Pout(indsTop(2,:)));
compression_2 = mean(Pout(indsTop(1,:)));

%% Flatten the later pulse: 2 variable optimum phase ramp
fprintf('Flattening of the latter pulse...\n');
x0 = [130, 2]; % start value
[x_opt,fval] = fminsearch(@(x) Chi2FlatPulse(x,pulse,cavity),x0);

pulse.phin = x_opt(1);
pulse.af = x_opt(2);
[Pin, Pout, U, indsTop] = CavityOutput(pulse, cavity);

compression_1 = mean(Pout(indsTop(2,:)));
compression_2 = mean(Pout(indsTop(1,:)));

t = pulse.t;
Ein = trapz(t, Pin);
E1 = trapz(t(indsTop(2,:)), Pout(indsTop(2,:)));
E2 = trapz(t(indsTop(1,:)), Pout(indsTop(1,:))); 
Eout = E1 + E2;
Efficiency = Eout/Ein;

compression_1 = mean(Pout(indsTop(2,:)))
compression_2 = mean(Pout(indsTop(1,:)))

end
