function [ ] = plotSignals(U, t, Pin, Pout, indsTop)
switch nargin
  case 4
    includeLines = false;
  case 5
    includeLines = true;
end

%% Extract amplitude and phase from complex signal:
u = abs(U);
f = atan2(imag(U),real(U)).*180/pi;

maxY = 1.1*max(Pout);
maxX = 10000;

%% Plot
hfig = figure;
subplot(1,2,1)
[ax, h1, h2] = plotyy(t,u, t,f);
set(h1, 'Linewidth', 2);
set(h2, 'Linewidth', 2);
hx = xlabel('time [ns]');
hy1 = ylabel(ax(1),'Amplitude');
hy2 = ylabel(ax(2),'Phase [degr.]');
axis(ax(1),[0, maxX, 0, 2]);
axis(ax(2),[0, maxX, 0, 180]);
set(ax(2),'YTick',[0 90 180]);
set([gca,hx, hy1, hy2, ax(1), ax(2)], 'Fontsize', 15);

subplot(1,2,2)
plot(t, Pin, 'Linewidth', 2, 'Color', 'b');
hold on;
plot(t, Pout, 'Linewidth', 2, 'Color', 'r');
if includeLines  
  if min(size(indsTop)) == 1
    plot([t(indsTop(1)), t(indsTop(1))], [0, maxY], 'k--');
    plot([t(indsTop(end)), t(indsTop(end))], [0, maxY], 'k--');
    plot([0, maxX], [mean(Pout(indsTop)), mean(Pout(indsTop))], 'k--');
  else
    plot([t(indsTop(1,1)), t(indsTop(1,1))], [0, maxY], 'k--');
    plot([t(indsTop(2,1)), t(indsTop(2,1))], [0, maxY], 'k--');
    plot([t(indsTop(1,end)), t(indsTop(1,end))], [0, maxY], 'k--');
    plot([t(indsTop(2,end)), t(indsTop(2,end))], [0, maxY], 'k--');
    plot([0, maxX], [mean(Pout(indsTop(1,:))), mean(Pout(indsTop(1,:)))], 'k--');
    plot([0, maxX], [mean(Pout(indsTop(2,:))), mean(Pout(indsTop(2,:)))], 'k--');
  end
end
hold off;
hx = xlabel('time [ns]'); 
hy = ylabel('Power');
hl = legend('Input', 'Output', 'Location', 'NorthWest');
set([gca,hx,hy,hl], 'Fontsize', 15);
set(hfig, 'Position', [195 408 1534 450]);
axis([0, maxX, 0, maxY]);

end

