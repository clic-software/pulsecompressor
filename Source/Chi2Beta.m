function chi2 = Chi2Beta(Beta, pulse, cavity)
% Goal function for finding the coupling that results in 
% maximizing the energy in the compressed pulse
[U, indsTop] = GenerateInputPulse(pulse);
t = pulse.t;
cavity.Beta = Beta;
[Pin, Pout] = CavityOutput(U, t, cavity);

if min(size(indsTop)) == 1
  Eout = trapz(t(indsTop), Pout(indsTop)); %integrate over compressed pulse
else 
  Eout = trapz(t(indsTop(1,:)), Pout(indsTop(1,:))) + ...
    trapz(t(indsTop(2,:)), Pout(indsTop(2,:))); %double pulse
end
chi2 = 1/Eout;   

end

