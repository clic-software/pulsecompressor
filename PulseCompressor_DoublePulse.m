% This script is originally based I. Syratchev's Mathcad tool
% Written by J. Ogren, 2018
clear all; close all;
addpath('./Source');
% Optimize the compression for a single pulse. For a given pulse length:
% 1) optimize beta to maximize the energy in the two compressed pulses
% 2) adjust pulse separation to make pulses equal
% 3) flatten the later pulse and then the former pulse
% 4) re-adjust pulse separation to make the two flat pulses equal

%% Define klystron pulse
pulse.tp = 800;                   % pulse length compressed pulse, ns
pulse.t = 5*1e-9*(0:2000)*1e9;    % time vector
pulse.TKp = 8000;                 % klystron pulse length, ns
pulse.Phb = 180;                  % Final phase
pulse.phin = 180;                 % Start of exponential ramp 
pulse.af = 1e-5;                  % "Curvature"
pulse.Phb2 = 180;                 % Final phase of former pulse
pulse.phin2 = 180;                % Start of exponential ramp of former pulse
pulse.af2 = 1e-5;                 % "Curvature" of former pulse
pulse.deltaT = 2000;              % separation between the two pulses

[U, indsTop] = GenerateInputPulse(pulse);
t = pulse.t;

%% Define cavity
cavity.Fc = 2e9; % cavity frequency
cavity.Q0 = 1.8e5;
cavity.Beta = 2;
cavity.fh = cavity.Fc - 50e6 + (0:2000)*0.05e6;

%% Generate output and plot
[Pin, Pout] = CavityOutput(U, t, cavity);
plotSignals(U, pulse.t, Pin, Pout);

compression_1 = mean(Pout(indsTop(2,:)))
compression_2 = mean(Pout(indsTop(1,:)))

%% Adjust beta to maximize energy in the two pulses
%% Optimize Beta
fprintf('\nOptimizing coupling...\n');
[Beta_opt,fval] = fminbnd(@(x) Chi2Beta(x,pulse,cavity),1,25)

cavity.Beta = Beta_opt;
[Pin, Pout] = CavityOutput(U, t, cavity);

% plot
plotSignals(U, pulse.t, Pin, Pout);

compression_1 = mean(Pout(indsTop(2,:)))
compression_2 = mean(Pout(indsTop(1,:)))

%% Adjust deltaT to make pulses equal:
fprintf('\nFind pulse separation to make pulses equal...\n');
LB = 100; % 100 ns lower boundary
UB = pulse.TKp - 2*pulse.tp - 100; % upper boundary
[deltaT_opt,fval] = fminbnd(@(x) Chi2EqualHeight(x,pulse,cavity),LB,UB)
pulse.deltaT = deltaT_opt;

[U, indsTop] = GenerateInputPulse(pulse);
[Pin, Pout] = CavityOutput(U, t, cavity);

plotSignals(U, pulse.t, Pin, Pout);

compression_1 = mean(Pout(indsTop(2,:)))
compression_2 = mean(Pout(indsTop(1,:)))

%% Flatten the former pulse: 2 variable optimum phase ramp
fprintf('\nFlattening of the former pulse...\n');
x0 = [130, 2]; % start value
[x_opt,fval] = fminsearch(@(x) Chi2FlatPulse_2(x,pulse,cavity),x0)

pulse.phin2 = x_opt(1);
pulse.af2 = x_opt(2);
[U, indsTop] = GenerateInputPulse(pulse);
[Pin, Pout] = CavityOutput(U, t, cavity);

compression_1 = mean(Pout(indsTop(2,:)))
compression_2 = mean(Pout(indsTop(1,:)))

%% Flatten the later pulse: 2 variable optimum phase ramp
fprintf('\nFlattening of the latter pulse...\n');
x0 = [130, 2]; % start value
[x_opt,fval] = fminsearch(@(x) Chi2FlatPulse(x,pulse,cavity),x0)

pulse.phin = x_opt(1);
pulse.af = x_opt(2);
[U, indsTop] = GenerateInputPulse(pulse);
[Pin, Pout] = CavityOutput(U, t, cavity);

compression_1 = mean(Pout(indsTop(2,:)))
compression_2 = mean(Pout(indsTop(1,:)))

Ein = trapz(t, Pin);
E1 = trapz(t(indsTop(2,:)), Pout(indsTop(2,:)));
E2 = trapz(t(indsTop(1,:)), Pout(indsTop(1,:))); 
Eout = E1 + E2;
Efficiency = Eout/Ein

%% Adjust deltaT to make pulses equal:
fprintf('\nAdjust pulse separation to make flat pulses equal...\n');
LB = 0.8*pulse.deltaT;
UB = 1.2*pulse.deltaT;
[deltaT_opt,fval] = fminbnd(@(x) Chi2EqualHeight(x,pulse,cavity),LB,UB)
pulse.deltaT = deltaT_opt;

[U, indsTop] = GenerateInputPulse(pulse);
[Pin, Pout] = CavityOutput(U, t, cavity);

plotSignals(U, pulse.t, Pin, Pout, indsTop);
plotSignals_power(pulse.t, Pin, Pout, indsTop);

compression_1 = mean(Pout(indsTop(2,:)))
compression_2 = mean(Pout(indsTop(1,:)))
