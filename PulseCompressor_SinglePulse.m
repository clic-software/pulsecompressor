% This script is based I. Syratchev's Mathcad tool
% Written by J. Ogren, 2018
clear all; close all;
addpath('./Source');
% Optimize the compression for a single pulse. For a given pulse length:
% 1) optimize beta to maximize the energy in the compressed pulse
% 2) find a ramp of the phase to achieve a flat pulse

%% Define klystron pulse
pulse.tp = 800;                   % pulse length compressed pulse, ns
pulse.t = 5*1e-9*(0:2000)*1e9;    % time vector
pulse.TKp = 8000;                 % klystron pulse length, ns
pulse.Phb = 180;                  % Final phase
pulse.phin = 180;                 % Start of exponential ramp
pulse.af = 2;                     % "Curvature"

[U, indsTop] = GenerateInputPulse(pulse);
t = pulse.t;

%% Define cavity
cavity.Fc = 2e9; % cavity frequency
cavity.Q0 = 1.8e5;
cavity.Beta = 5;
cavity.fh = cavity.Fc - 50e6 + (0:2000)*0.050e6;


%% Generate output and plot
[Pin, Pout] = CavityOutput(U, t, cavity);
u = abs(U);
f = atan2(imag(U),real(U)).*180/pi;
plotSignals(U, t, Pin, Pout); 

compression = mean(Pout(indsTop))
Ein = trapz(t, Pin);
Eout = trapz(t(indsTop), Pout(indsTop));
Efficiency = Eout/Ein

%% Optimize Beta
fprintf('\nOptimizing coupling...\n');
O = optimset('TolFun', 1e-6, 'TolX', 1e-6, 'MaxFunEvals', 50, 'MaxIter', 100);
[Beta_opt,fval] = fminbnd(@(x) Chi2Beta(x, pulse, cavity),1,25,O)
cavity.Beta = Beta_opt;
[Pin, Pout] = CavityOutput(U, t, cavity);

% plot
plotSignals(U, t, Pin, Pout);
compression = mean(Pout(indsTop))
Ein = trapz(t, Pin);
Eout = trapz(t(indsTop), Pout(indsTop));
Efficiency = Eout/Ein


%% Flatten pulse: 2 variable optimum phase ramp
fprintf('\nFlattening of the pulse...\n');
x0 = [120, 1.7]; % start value
[x_opt,fval] = fminsearch(@(x) Chi2FlatPulse(x, pulse, cavity),x0,O)

pulse.phin = x_opt(1);
pulse.af = x_opt(2);
[U, indsTop] = GenerateInputPulse(pulse);
[Pin, Pout] = CavityOutput(U, t, cavity);

% plot
plotSignals(U, t, Pin, Pout, indsTop);
plotSignals_power(t, Pin, Pout, indsTop); % plot only power signals

compression = mean(Pout(indsTop))
Ein = trapz(t, Pin);
Eout = trapz(t(indsTop), Pout(indsTop));
Efficiency = Eout/Ein

